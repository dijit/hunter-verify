extern crate postgres;

#[allow(unused_imports)]
use log::{debug, error, log_enabled, info, trace, Level};
use std::thread;
use postgres::{Client, NoTls};
use std::sync::mpsc::{channel, Sender, Receiver};

// FIXME: Hardcoded DB Connection String
const DB_CONN_STR: &str = "postgres://dijit@localhost:5432/dijit";

fn verify_data(_shard: i16, tx: Sender<u16>) {
    let conn = Client::connect(DB_CONN_STR, NoTls).unwrap();
    debug!("[{}] Beginning database sweep, only checking 'current' saves", _shard);
    let active_records = backup_verify::get_database_active_records(conn, _shard);
    

    trace!("[{}] dicky contains {}", _shard, active_records.len());
    let _ = tx.send(active_records.len() as u16);
    trace!("[{}] Active Records first entry is {}", _shard, active_records[0]);
    trace!("[{}] Active Records first entry has a length of {}", _shard, active_records[0].len());
    trace!("[{}] Active Records first entry has capacity of {}", _shard, active_records[0].capacity());
}

fn receiver(c: Receiver<u16>) {
    let mut collector = 0;
    loop {
        match c.try_recv() {
            Ok(r) => {
                    collector = collector + r;
                    trace!("Received: {}", r);
                    trace!("Count is now: {}", collector);
                }
            // FIXME: takes roughly 10s after dropping for this to be noticed.
            Err(std::sync::mpsc::TryRecvError::Disconnected) => {
                trace!("Terminating.");
                return;
            }
            Err(std::sync::mpsc::TryRecvError::Empty) => {}
        }
    }
}


fn main() {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("warn")).init();
    
    let mut children = vec![];
    let conn = Client::connect(DB_CONN_STR, NoTls).unwrap();
    let shards = backup_verify::get_shard_count(conn).unwrap();
    debug!("Total shards: {}", shards);

    let (tx, rx) = channel();
    let rx_handle = thread::spawn(move || receiver(rx));
    for shard in 0i16..shards+1 {
        let tx = tx.clone();
        children.push(
            thread::spawn(move || {
                verify_data(shard, tx)
            }));
    }

    thread::sleep(std::time::Duration::from_secs(10));
    for child in children {
        let _ = child.join();
    };
    
    drop(tx);
    let _ = rx_handle.join();
}

