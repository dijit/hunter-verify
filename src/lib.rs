use postgres::Client as pgclient;

#[allow(unused)]
struct SaveGame {
    #[allow(dead_code)]
    account_id: i64,
    file_path: String,
    #[allow(dead_code)]
    shard_index: i16,
}

// NOTE: I could have used an array here instead of a Vec, since Vec's are heap allocated and thus 
//       marginally slower to access. However, since TCTD as more than 10M players, I'm not sure
//       that the number of active saves will live comfortably on the stack, especially with a low
//       number of shards. We also have to assume that someone uses only 1 shard at some point.
pub fn get_database_active_records(mut dbconn: pgclient, shard_index: i16) -> Vec<String> {
    // Start TX
    // Count all active rows;
    let row = &dbconn.query_one(
        "SELECT count(*) FROM save_games where current = true and shard_index = $1", 
        &[&shard_index])
        .unwrap();
    let rowcount: i64 = row.get(0);
    let mut collector= Vec::with_capacity(rowcount as usize);
    // Allocate sized Vec of str with the right length of an uuid
    for row in &dbconn.query(
            "SELECT file_path FROM save_games where current = true and shard_index = $1",
            &[&shard_index]).unwrap() {
        let mut stringy = String::with_capacity(35); // 36 is the maximum size of a UUID
        stringy.insert_str(0, row.get(0));
        collector.push(stringy);
    }
    // Return the Vec
    collector
}

pub fn get_shard_count(mut dbconn: pgclient) -> Result<i16, ()> {
    let row = &dbconn.query_one(
        "SELECT max(shard_index) FROM save_games where current = true", 
        &[]).unwrap();
    let num: i16 = row.get(0);
    Ok(num)
}

fn does_file_exist(file: &str, tank: &str) -> Result<bool, std::io::Error> {
    let path = format!("/{}/{}", tank, file);
    Ok(std::path::Path::new(&path).exists())
}
